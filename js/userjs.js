$(document).ready(function(){
/*把手刻的footer放到預設footer內*/
    $('footer .pull-left.copyright').before($('footer.container-fluid.foot'));
    
    //移除預設無連結點擊功能
       $("[class^='col-md-12'] img").parent("a[href='']").removeAttr('href');
       $("[class^='col-md-12'] img").parent("a[href='#']").removeAttr('href');
       $("[class^='col-md-12']  a[href='http://www.shortday.in/wp-content/uploads/2015/05/emma-stone-beautiful-wallpaper2.jpg']").removeAttr('href');
     if ($(window).width() < 767) {
          //三層選單
          (function($){
              $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
                event.preventDefault(); 
                event.stopPropagation(); 
                $(this).parent().siblings().removeClass('open');
                $(this).parent().toggleClass('open');
              });
          })(jQuery);

          $('.foot .row .col-sm-3:first-child').after($('.foot .row .col-sm-3:last-child'))
      };  
  })